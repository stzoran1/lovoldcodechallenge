﻿## Running the application

In order to run application please install Visaul Studio 2019 and ensure .NET Core 3.1 SDK is installed.
When this is installed, open command line and navigate to root of the project and then run: 

```dotnet run```

for development you can use:

```dotnet watch run```

## Application settings

All settings should be defined in **appsettings.json** which is not commited in this repository due to security reasons.
In order to make your local settings please rename **appsettings.example** to **appsettings.json** and set desired values according to your local environment.

## Creating Database from migrations
```dotnet ef database update```
