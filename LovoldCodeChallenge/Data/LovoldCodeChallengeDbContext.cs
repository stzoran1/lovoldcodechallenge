﻿using LovoldCodeChallenge.Models;
using Microsoft.EntityFrameworkCore;

namespace LovoldCodeChallenge.Data
{
    public class LovoldCodeChallengeDbContext : DbContext
    {
        public LovoldCodeChallengeDbContext(DbContextOptions<LovoldCodeChallengeDbContext> options) : base(options) { }

        public DbSet<Customer> Customer { get; set; }

    }
}
