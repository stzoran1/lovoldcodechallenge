﻿
using System.ComponentModel.DataAnnotations;
namespace LovoldCodeChallenge.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }

        [Required]
        [Display(Name = "Company Name", Prompt = "Enter Company Name", Description = "Company Name")]
        [StringLength(100)]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "First Name", Prompt = "Enter First Name", Description = "First Name")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name", Prompt = "Enter Last Name", Description = "Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", Prompt = "Enter Email", Description = "Email")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number", Prompt = "Enter Phone Number", Description = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Position", Prompt = "Enter Position", Description = "Position")]
        [StringLength(100)]
        public string Position { get; set; }

    }
}
