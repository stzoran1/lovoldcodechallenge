﻿var table;
$(function () {
    table = $('#customersTable').DataTable({
        // Design Assets
        stateSave: true,
        autoWidth: true,
        // ServerSide Setups
        processing: true,
        serverSide: true,
        // Paging Setups
        paging: true,
        // Searching Setups
        searching: { regex: true },
        // Ajax Filter
        ajax: {
            url: "/Customers/LoadTable",
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            data: function (d) {
                return JSON.stringify(d);
            }
        },
        // Columns Setups
        columns: [
            { data: "companyName" },
            { data: "firstName" },
            { data: "lastName" },
            { data: "email" },
            { data: "phoneNumber" },
            { data: "position" },

        ],
        // Column Definitions
        columnDefs: [
            { targets: "no-sort", orderable: false },
            { targets: "no-search", searchable: false },
            {
                targets: "trim",
                render: function (data, type, full, meta) {
                    if (type === "display") {
                        data = strtrunc(data, 10);
                    }

                    return data;
                }
            },
            { targets: "date-type", type: "date-eu" },
            {
                "targets": 6,
                "data": null,
                "defaultContent": "<a class='btn btn-link' role='button' href='#' onclick='details(this)' data-toggle='tooltip' data-placement='top' title='Details'><i class='fa fa-eye'></i></a>",
                "orderable": false
            },

            {
                "targets": 7,
                "data": null,
                "defaultContent": "<a class='btn btn-link' role='button' href='#' onclick='edit(this)' data-toggle='tooltip' data-placement='top' title='Edit'><i class='fa fa-pencil'></i></a>",
                "orderable": false
            },

            {
                "targets": 8,
                "data": null,
                "defaultContent": "<a class='btn btn-link' role='button' href='#' onclick='remove(this)' data-toggle='tooltip' data-placement='top' title='Delete'><i class='fa fa-times'></i></a>",
                "orderable": false
            },
        ]
    });
});


function details(elem) {

    var data = table.row($(elem).parents('tr')).data();
    window.location = '../../../../Customers/Details/' + data['customerID'];

}

function edit(elem) {

    var data = table.row($(elem).parents('tr')).data();
    window.location = '../../../../Customers/Edit/' + data['customerID'];

}

function remove(elem) {

    var data = table.row($(elem).parents('tr')).data();
    window.location = '../../../../Customers/Delete/' + data['customerID'];

}


function strtrunc(str, num) {
    if (str.length > num) {
        return str.slice(0, num) + "...";
    }
    else {
        return str;
    }
}